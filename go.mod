module gitlab.com/edthamm/trelloMonteCarlo

go 1.19

require github.com/securego/gosec v0.0.0-20200401082031-e946c8c39989

require (
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/montanaflynn/stats v0.6.6 // indirect
	github.com/spf13/cobra v1.6.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/tools v0.0.0-20200331202046-9d5940d49312 // indirect
)
