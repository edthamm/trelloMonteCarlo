package main

import (
	"fmt"
	"math/rand"
	"time"

	stats "github.com/montanaflynn/stats"
	"github.com/spf13/cobra"
)

var tasks int
var startDateString string

func init() {
	rootCmd.Flags().IntVar(&tasks, "tasks", 0, "The amount of open tasks")
	rootCmd.Flags().StringVar(&startDateString, "startDate", "", "The date to start work on. Format: YYYY-MM-DD Default: Today")
}

func main() {
	rootCmd.Execute() // #nosec - errors in main are allowed to bubble up in this case
}

var rootCmd = &cobra.Command{
	Use:   "trelloMonteCarlo",
	Short: "Runs a Monte Carlo simulation based on the cards in a Trello Board",
	Long: `With this tool you can, given your work history in the form of cards on a Trello
board, calculate when x tasks will be done with a certain probability.`,
	Run: testing,
}

func testing(cmd *cobra.Command, args []string) {
	simIn := SimulationInput{
		tasks: tasks,
		past:  []int{0, 0, 3, 2, 0, 8, 0, 0, 0, 1, 2, 2, 0, 0, 0, 0},
		runs:  500,
	}

	if startDateString != "" {
		parsed, err := time.Parse("2006-01-02", startDateString)
		if err != nil {
			fmt.Println("Date string must have format YYYY-MM-DD and be a valid date Example: 2022-01-31")
			cobra.CheckErr(err)
			// TODO better bailout
		}
		simIn.startDate = parsed
	} else {
		simIn.startDate = time.Now()
	}

	res := simulate(simIn)

	fmt.Printf("If you start work in the  %s. You will finish: \n", simIn.startDate)
	fmt.Printf("80th Percentile: %s \n", simIn.startDate.Add(time.Hour*24*time.Duration(res.p80)))
	fmt.Printf("90th Percentile: %s \n", simIn.startDate.Add(time.Hour*24*time.Duration(res.p90)))
	fmt.Printf("95th Percentile: %s \n", simIn.startDate.Add(time.Hour*24*time.Duration(res.p95)))
	fmt.Printf("99th Percentile: %s \n", simIn.startDate.Add(time.Hour*24*time.Duration(res.p99)))
}

// TODO
// Input params:
// * Trello board and auth data
// * Look back time
// * Number of tickets
// * Start date
// * Number of simulations to run
// * (phase 2) factor to apply
//
// Output:
// * Histogram containing the dates for the completion with the
// * 80,90,95,99 percentile probability
// * In phase 1 this could also be a table
//
// Work
// * Get slice of completed tickets per day from Trello for look back time
// * Display results (as finish dates == start date + time to finish)
type SimulationInput struct {
	past      []int
	tasks     int
	runs      int
	startDate time.Time
}
type SimulationResults struct {
	p80 float64
	p90 float64
	p95 float64
	p99 float64
}

func simulate(in SimulationInput) SimulationResults {
	durations := []float64{}

	for i := 0; i < len(in.past); i++ {
		durations = append(durations, simulateSingleRun(in.past, in.tasks))
	}

	p80, err := stats.Percentile(durations, 80)
	cobra.CheckErr(err)
	p90, err := stats.Percentile(durations, 90)
	cobra.CheckErr(err)
	p95, err := stats.Percentile(durations, 95)
	cobra.CheckErr(err)
	p99, err := stats.Percentile(durations, 99)
	cobra.CheckErr(err)

	return SimulationResults{p80: p80, p90: p90, p95: p95, p99: p99}
}

func simulateSingleRun(past []int, tasks int) float64 {
	days := 0.0

	for tasks >= 0 {
		days += 1
		i := selectDayFromPast(past)
		tasks -= past[i]
	}

	return days
}

func selectDayFromPast(past []int) int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(len(past))
}
