#! /bin/sh

mkdir -p .cover
PKG_LIST=$(go list ./... | grep -v /vendor/)

for package in ${PKG_LIST}; do
    go test -covermode=count -coverprofile ".cover/${package##*/}.cov" "$package" ;
done

tail -q -n +2 .cover/*.cov >> .cover/coverage.cov

if [ -z "$1" ]; then
  go tool cover -func=.cover/coverage.cov
  return
fi

if [ $1 = "html" ]; then
  go tool cover -html=.cover/coverage.cov -o .cover/coverage.html
  return
fi
